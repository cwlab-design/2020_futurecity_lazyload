$(
  ".owl-carousel .item:nth-child(n+2) img, .main img, .tab-wrapper .tab-content:nth-child(n+6) .dosearch-article .dosearch-left img, .nevin img, .channalGroup:nth-child(n+3) img, .global-box .thumb:nth-child(n+4) img"
).addClass("lazyload");

$(".feature__page .container img").addClass("lazyload");

$(function() {
  if ($(window).width() > 650) {
    $(".indexKv_mb").remove();
  }else{
    $(".indexKv_pc").remove();
  }
});